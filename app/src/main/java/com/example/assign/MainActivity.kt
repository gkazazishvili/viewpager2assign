package com.example.assign

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.assign.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
         init()


    }

    private fun init(){
        val images = listOf(

            R.drawable.ker1,
            R.drawable.ker2,
            R.drawable.ker3,
            R.drawable.ker4,
            R.drawable.ker5
        )

        val title = listOf(
            "First Image",
            "Second Image",
            "Third Image",
            "Fourth Image",
            "Fifth Image",
        )

        val description = listOf(
            "First Image is all about hotdog",
            "Second Image is all about bidzina",
            "Third Image is all about leqso",
            "Fourth Image is all about jinoria",
            "Fifth Image is all about trezora",
        )

        val adapter = PgAdapter(images, title, description)
        binding.viewPager.adapter = adapter
    }

}
