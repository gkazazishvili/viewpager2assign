package com.example.assign

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class PgAdapter(
    private val images: List<Int>,
    private val title: List<String>,
    private val description: List<String>
) : RecyclerView.Adapter<PgAdapter.ViewPageViewHolder>(){

    inner class ViewPageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewPageViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_view_pager, parent, false)
        return ViewPageViewHolder(view)
    }

    override fun getItemCount(): Int {
        return images.size
    }

    override fun onBindViewHolder(holder: ViewPageViewHolder, position: Int) {
        val curTitle = title[position]
        val curDesc = description[position]
        val curImages = images[position]
        val imgSize = images.size.toString()
        val position:Int = position+1
        val curItem:String = "Current Position: $position"
        val sizeOfImages:String = "Size of all images: $imgSize"


        holder.itemView.findViewById<ImageView>(R.id.ivImage).setImageResource(curImages)
        holder.itemView.findViewById<TextView>(R.id.title).text = curTitle
        holder.itemView.findViewById<TextView>(R.id.description).text = curDesc
        holder.itemView.findViewById<TextView>(R.id.curPosition).text = curItem
        holder.itemView.findViewById<TextView>(R.id.imgSize).text = sizeOfImages


    }


}